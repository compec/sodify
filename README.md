# Sodify

## ¿Cómo ver Sodify?

* **Descarga este repositorio**. En el panel de la derecha, exactamente en el campo Size, hay un enlace de descarga.
* **Abre la carpeta descargada** (que probablemente está contenida en un archivo comprimido).
* **Navega hacia el directorio** –o presentación– de tu preferencia y dale doble clic –o **abre– el archivo `index.html`**. Asegúrate que el HTML sea manejado por un navegador de vanguardia.
* Alternativamente, utiliza los PDFs alojados en los directorios de cada presentación.

## ¿Cómo seguir Sodify?

* Lo ideal es ver las presentaciones _full-featured_ en el navegador y luego leer los slides con las notas de presentador –muy ilustradoras, por cierto.
* También deberías seguir los ejercicios de final de capítulo (aquellos que tienen cubierta amarilla).
* Por último y para cerrar la sesión apropiadamente deberías avanzar con los ejercicios o las lecturas sugeridas al final de la presentación. Estas dos son muy recomendables:
    - [Everything you need to know about the internet — Vox](http://www.vox.com/cards/the-internet/what-is-the-internet)
    - [In the programmable world, all our objects will act as one (Internet of Things) — Wired](http://www.wired.com/2013/05/internet-of-things-2/all/)

## No quiero descargar, abrir y todo eso, ¿no tienes una forma más simple?

Sí, tengo links que apuntan directamente a las **presentaciones en iCloud**. Bastante simple. Sin embargo, ahí no encontrarás las notas de presentador... que, de hecho, son vitales y constituyen una parte importante del aprendizaje.

* [Internet y la web](https://www.icloud.com/iw/#keynote/BAJe91G0ttgx7Usly8WB5C4EwMonJdsbZkCF/Sodify_-_Internet_y_la_web.key)
* [Texto plano y estándares de la web](https://www.icloud.com/iw/#keynote/BALDwVq5kCm2SrHYMsGBaA_6PyxTAtK9RC6F/Sodify_-_Texto_plano_y_esta%CC%81ndares_de_la_web.key)
* [Comunicación y lenguajes en el servidor](https://www.icloud.com/iw/#keynote/BALfuUVYbrm5yFxQsJSBY_7NEa5dWGnoXlCF/Sodify_-_Comunicacio%CC%81n_y_lenguajes_en_el_servidor.key)
* [Sitios estáticos vs. dinámicos](https://www.icloud.com/iw/#keynote/BAJ_lMGVpw4tTZ-Us_OBpHnXm1-HPclIp26F/Sodify_-_Sitios_esta%CC%81ticos_vs._dina%CC%81micos.key)
* [Express](https://www.icloud.com/iw/#keynote/BAIAxeHmlOWJzQnPRkKB6QlC2LIpug09eZmF/Sodify_Express.key)

## ¿Cómo configurar una Mac para Sodify?

Para seguir los últimos "Vamos a la acción" de Sodify debes tener Apache (servidor web) y PHP (lenguaje de programación para la web) corriendo sin problemas. Con este propósito en mente, debes hacer esto rápidamente:

1. Abre Finder y tipea `cmd+shift+G` (Go > Go to Folder...).
2. En la caja de entrada de texto copia y pega `/Library/WebServer/Documents`.
3. Ahora estás en la carpeta indicada. Aquí pondrás todo lo que quieras que salga en "tu web".
4. Puedes eliminar todo lo que hayas encontrado en esta carpeta (no sirve, en realidad).
5. Puedes poner en su lugar `index.html` que se encuentra en la carpeta `templates` de este repositorio.
5. Dirígete a [localhost](http://localhost/) y _ka-boom_ ya tienes tu servidor web listo.
6. Lo único que falta es habilitar PHP. He creado un [artículo específicamente con este objetivo](http://blog.juandiegogonzales.com/enable-php-apache-os-x/).